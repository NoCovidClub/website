# NoCovid.Club Website

Powered by [Hugo](https://gohugo.io/).

## Setup

```bash
git clone --recurse-submodules URL DIR
cd DIR
git config user.name NoCovidClub
git config user.email nocovidclub@noreply.codeberg.org
hugo server
```

## Deployment

```bash
rm -rf public/
hugo
git checkout pages
rsync -ai --delete --filter ': .rsync-filter' public/ .
git add -A
git commit -am $(date -u -Iseconds)
git push
git checkout main
```
