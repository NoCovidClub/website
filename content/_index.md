---
title: NoCovid.Club
date: 2023-04-28T14:26:11+02:00
layout: single
keywords: [coronavirus, sars-cov-2, covid-19, news, science, prevention, public health]
---

Protect yourself and others, because [#CovidIsNotOver](https://mastodon.social/tags/CovidIsNotOver)! 😷✌️

Comprehensive resources to keep you informed and safe:

## News

- 📰 [Threat Model](https://www.patreon.com/collection/1160) (Violet Blue)<br>
  *An inclusive, curated selection of new information and fresh perspectives on Covid-19 published every Thursday.*
- 📺 [The People’s Health Briefing](https://www.youtube.com/@RootsCommunityHealth/streams) (Roots Community Health)<br>
  *The goal of The People's Health Briefing is to empower you with our best knowledge and thinking every Tuesday morning.*
- 🎧 [Still Here](https://thesicktimes.org/category/podcast/) (The Sick Times)<br>
  *Still Here is an abridged version of The Sick Times’ newsletter, which publishes weekly.*

## Community

- [Covid Meetups](https://covidmeetups.com/)<br>
  *A free service to find individuals, families and local businesses/services who take COVID precautions in your area.*
- [Covid Action Map](https://covidactionmap.org/)<br>
  *Mapping COVID action groups worldwide.*

## Science

- 2024-05-14: [Honesty About Covid is Essential for Progress](https://johnsnowproject.org/primers/honesty-about-covid-is-essential-for-progress/) (The John Snow Project)
- 2024-03-30: [The Greatest Trick](https://johnsnowproject.org/primers/the-greatest-trick/) (The John Snow Project)
- 2023-08-22: [Is the Vaccine-Only Strategy Sustainable?](https://johnsnowproject.org/primers/is-the-vaccine-only-strategy-sustainable/) (The John Snow Project)

## Receipts

- 2024-11-07: [COVID-19 & The 2024 Election](https://www.panaccindex.info/p/covid-19-and-the-2024-election) (Pandemic Accountability Index)
- 2023-12-30: [Pandemic Roundup: 2023 year in review](https://www.patreon.com/posts/pandemic-roundup-95546410) (Violet Blue)
- 2023-12-23: [How the press manufactured consent](https://www.thegauntlet.news/p/how-the-press-manufactured-consent) (Julia Doubleday)
